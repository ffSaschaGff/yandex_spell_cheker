"use strict"
function YandexSpellCheker(elem) { 
	this.checkText = async function() {
		let response = await fetch('https://speller.yandex.net/services/spellservice.json/checkText?text='+encodeURI(elem.textContent));
		let jsonResult = await response.json();
		setOptionsIntoText(jsonResult);
	};
	
	this.hideContextMenu = function() {
		let contextMenu = document.getElementById("yandexSpellChekerContext");
		if (contextMenu) contextMenu.remove();
	}
	elem.yandexSpellCheker = this;
	elem.addEventListener("contextmenu", onContextMenu);

	this.onErrorClick = function() {
		console.dir(this);
	}
	
	//событие по вызову контекст меню на поле
	function onContextMenu(e) {
		e.preventDefault();
		let contextmenu = getMainContexMenu({x:e.offsetX,y:e.offsetY});
	}
	
	//получает контекст меню
	function getMainContexMenu(settings) {
		let container = document.getElementById("yandexSpellChekerContext") || document.createElement("div");
		container.id="yandexSpellChekerContext"
		container.innerHTML = "";
		
		let checkButton = document.createElement("button");
		checkButton.addEventListener("click",function() {
				elem.yandexSpellCheker.checkText();
				elem.yandexSpellCheker.hideContextMenu();
			});
		checkButton.append("Проверить орфографию");
		container.append(checkButton);
		
		container.style.zIndex = 10000;
		container.style.position = "absolute";
		container.style.top = settings.y+"px";
		container.style.left = settings.x+"px";
		document.body.append(container);
		return container;
	}
	
	function setOptionsIntoText(yandexErrors) {
		console.log(yandexErrors);
		let nodesToAdd = [];
		//if (yandexErrors.length()!=0&&yandexErrors[0].pos!=0) nodesToAdd.push(elem.textContent.substr(0,yandexErrors[0].pos));
		let lastIndex = 0;
		for (let error of yandexErrors) {
			nodesToAdd.push(elem.textContent.slice(lastIndex,error.pos));
			let span = document.createElement("span");
			span.setAttribute("data-options",[error.word,...error.s].join(","));
			span.setAttribute("data-pos",0);
			span.className="needToCheck_error";
			span.append(error.word);
			nodesToAdd.push(span);
			lastIndex = error.pos + error.len;
			span.addEventListener("click",YandexSpellCheker.onClickWrongWord);
		}
		nodesToAdd.push(elem.textContent.substr(lastIndex));
		
		elem.innerHTML = "";
		elem.append(...nodesToAdd);
	}
};
YandexSpellCheker.onClickWrongWord = function(event) {
	var options = event.target.dataset.options.split(",");
	event.target.dataset.pos++;
	if (event.target.dataset.pos == options.length) {
		event.target.dataset.pos = 0;
	}
	event.target.className= event.target.dataset.pos == 0 ? "needToCheck_error" : "needToCheck_correct";
	event.target.textContent = options[event.target.dataset.pos];
}

//по клику вне контекстного меню закрыть его
document.addEventListener("click", function(e) {
	let contextMenu = document.getElementById("yandexSpellChekerContext");
	if (!contextMenu) return;
	
	if (!targetIsMenu(e.target,contextMenu)) contextMenu.remove();
	
	function targetIsMenu(elem, menu) {
		if (elem == menu) return true;
		else if (elem == null) return false;
		else return targetIsMenu(elem.parentNode,menu);
	}
});